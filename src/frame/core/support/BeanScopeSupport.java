package frame.core.support;

import frame.core.BeanWrapper;

public class BeanScopeSupport {

	public static BeanWrapper getPrototypeBean(ThreadLocal<BeanWrapper> tl) {
		return tl.get();
	}
	
	public static void setPrototypeBean(ThreadLocal<BeanWrapper> tl, BeanWrapper bw) {
		tl.set(bw);
	}
	
}
